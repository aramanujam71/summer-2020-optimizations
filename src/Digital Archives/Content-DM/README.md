# Using Content DM #
###  *A breakdown of existing ContentDM API Functionality* ###
### A couple of notes: ###
* These scripts *may* need to updated
* Whenever prompted for the server address that is **17304**
* Whenever prompted for the site url that is **http://d.library.unlv.edu**
## API Functionality: ##
* API: dmGetCollectionParameters
    * This API translates an alias (linl) into a name (Springfield Aviation Company Collection) for a collection.
* API: dmGetCollectionList
    * When invoked returns a list of all collections into an XML
* API: dmGetCollectionFieldInfo
    * CONTENTdm uses nicknames for the metadata held in its records. The nicknames may or may not correspond to the actual field name. For instance, the title field might have a nickname of title, but Subject – LCSH might be called subjeca. Translating the field names is a job for    dmGetCollectionFieldInfo.
* API: dmQuery
    * alias is a single collection alias, a !-delimited list of collection aliases, or “all” for all collections.
    * searchstrings is a four-part, ^-delimited group in the order field^string^mode^operator.Use “CISOSEARCHALL” for all fields; mode can be “all”, “any”, “exact”, or “none”; operator can be “and” or “or”.Multiple words in string need to be separated by “+”.Up to six groups can be included, delimited by “!”.To browse a collection, specify a single alias and “0” as a searchstrings value. The operator for the last searchstring will be ignored.
    * fields is a !-delimited list of field nicknames, listing the fields for which metadata should be returned. A maximum of five fields may be specified. A maximum of 100 bytes is returned for each field.
    * sortby is a !-delimited list of field nicknames, detailing how the result should be sorted, in field order. The field nicknames must appear in the field array. If the last element in the array is specified as “reverse”, the sort will be in reverse (descending) order. Use “nosort” to sort the query by relevance.
    * maxrecs is the maximum number of records to return, from 1 to 1024.
    * start is the starting number of the first item returned in the result.
    * suppress specifies whether to suppress compound object pages from the search. Use “1” to suppress or “0” to not suppress (i.e., include pages).
    * docptr specifies the pointer value of a compound object to restrict the query just to the pages in that compound object. This requires that a single alias be specified. Use “0” if not specified.
    * suggest specifies whether to return a spelling suggestion, if available for the searchstring‘s string. Use “1” to get suggestions or use “0” to not return a suggestion.
    * facets is an optional !-delimited list of field nicknames to return as facets. Use “0” if not requesting facets.
    * showunpub specifies whether to show or not show items from unpublished collections. Use “1” if requesting items from unpublished collections or “0” to hide items from unpublished collections.
    * denormalizeFacets specifies whether to show capitalization and diacritics in facet fields that have shared Controlled Vocabulary. Use “1” if requesting capitalization and diacritics or “0” to turn off capitalization and diacritics.
    * format is either “xml” or “json”.
* API: dmGetItemInfo
    * Returns a full list of item parameters in an XML
* API: dmGetThumbnail
    * One of the two image APIs
    * Note those APIs make direct calls to the underlying file system not the web service
    * *MAY BE DEPRECIATED LOOKING INTO IT*
* API: dmGetImage
    * One of the two image APIs
    * Note those APIs make direct calls to the underlying file system not the web service
    * *MAY BE DEPRECIATED LOOKING INTO IT*
* API: dmBrowse
    * Looks through all of the collections with a search term



