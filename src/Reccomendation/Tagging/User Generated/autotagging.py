import nltk
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
from nltk.corpus import wordnet
import numpy as np
import os
from rake_nltk import Rake

# This function will get all of the nouns from a raw text file using NLTK; the nouns don't include stopwords which are words that add no contextual value
# The file format is completely trivial and in the future it can support any raw text per implementation details
def noun_search(file):
    if os.stat(file).st_size == 0:
        raise OSError("The file you are trying to search is empty")
    stop_words = set(stopwords.words('english'))
    try:
        f = open(file)
        raw = f.readline()
        is_noun = lambda pos: pos[:2] == 'NN'
        tokenized = nltk.word_tokenize(raw)
        nouns = [word for (word, pos) in nltk.pos_tag(tokenized) if is_noun(pos)]
        filtered_nouns = []
        for w in nouns:
            if w not in stop_words and len(w) > 4 and w not in filtered_nouns:
                filtered_nouns.append(w)
        return filtered_nouns
    finally:
        f.close()

# Using the compare_words helper function this function is able to take two files and compare all of their words, it only indexes those with a similarity index of more than 0.75
# which is the conventional benchmark for similarity
def compare_all(file_one, file_two):
    sim_words = {}
    nouns_one = noun_search(file_one)
    nouns_two = noun_search(file_two)
    for word_one in nouns_one:
        for word_two in nouns_two:
            sim_index = compare_words(word_one, word_two)
            if sim_index > 0.75:
                sim_words.update({(word_one, word_two) : sim_index})
    return sim_words

# This is a helper method that uses the Wu-Palmer similarity test to compare single words
def compare_words(word_one, word_two):
    if not word_one or not word_two:
        raise OSError("One of the words to be compared was empty")
    synset_one = wordnet.synsets(word_one)[0]
    synset_two = wordnet.synsets(word_two)[0]
    sim_index = synset_one.wup_similarity(synset_two)
    if sim_index is None:
        return 0
    else:
        return sim_index
      
# The Rake function from the natural language toolkit can extract key-phrases and words from any piece of text.
def key_phrases(file):
    if os.stat(file).st_size == 0:
        raise OSError("The file you are trying to search is empty")
    try:
        txt = open(file)
        raw = txt.readline()
        r = Rake()
        r.extract_keywords_from_text(raw)
        keywords = r.get_ranked_phrases()
        key_phrases = keywords[:10]
        return key_phrases
    finally:
        txt.close()

# Suggests tags for a file based on the intersection of nouns and keywords
def suggest_tags(file):
    if os.stat(file).st_size == 0:
        raise OSError("The file you are trying to search is empty")
    nouns = noun_search(file)
    raked_phrases = key_phrases(file)
    suggested_tags = []
    for noun in nouns:
        for phrase in raked_phrases:
            if noun in phrase:
                suggested_tags.append(noun)
    return suggested_tags



# Assigns parts of speech tags to all key phrases in a file
def tag_phrases(file):
    stop_words = set(stopwords.words('english'))
    phrases = key_phrases(file)
    tagged_list = []
    for phrase in phrases:
        tokened_phrase = phrase.split()
        full_sentence = find_sentence(phrase, file)
        tagged_sentence = tag(full_sentence)
        tagged_phrase = []
        for word_tag in tagged_sentence:
            if (word_tag[0][0] in tokened_phrase):
                tagged_phrase.append(word_tag[0])
            elif (word_tag[0][0].lower() in tokened_phrase):
                tagged_phrase.append((word_tag[0][0].lower(), word_tag[0][1]))
        tagged_phrase = list( dict.fromkeys(tagged_phrase))
        tagged_list.append({phrase: tagged_phrase})
    return  tagged_list

# Helper function used to find the complete sentence any phrase belongs to
def find_sentence(phrase, file):
    f = open(file)
    try:
        data = f.read()
        sentences = data.split(". ")
        for sentence in sentences:
            if (phrase in sentence):
                return sentence
            if (phrase.capitalize() in sentence):
                return sentence
    finally:
        f.close()

# Helper function used to tag any piece of text (better used for long-form) minimal unit should be a sentence
def tag(corpus):
    stop_words = set(stopwords.words('english'))
    tokens = corpus.split()
    tags = []
    for i in tokens:
        words = nltk.word_tokenize(i)
        words = [w for w in words if not w in stop_words]
        tags.append(nltk.pos_tag(words))
    removed_empties = []
    for tag in tags:
        if not tag:
            continue
        else:
            removed_empties.append(tag)
    return removed_empties

# This is the heavy lifting in our phrase similarity metric utilizes conceptual similarity and semantic similiarity
# TO DO
# Some issues with the context of each vectors meaning where they come from
def compare_phrases(phrase_one, phrase_two, file_one, file_two):
    full_content_one = tag_phrases(file_one)
    full_content_two = tag_phrases(file_two)
    phrase_context_one = {}
    phrase_context_two = {}
    synsets_one = {}
    synsets_two = {}
    for dictionary in full_content_one:
        if list(dictionary.keys())[0] == phrase_one:
            phrase_context_one = dictionary
            break
    for dictionary in full_content_two:
        if list(dictionary.keys())[0] == phrase_two:
            phrase_context_two = dictionary
            break
    sto = []
    for tupl in list(phrase_context_one.values())[0]:
        sto.append(wordnet.synsets(tupl[0])[0])
    synsets_one.update({list(phrase_context_one.keys())[0]: sto})
    sto = []
    for tupl in list(phrase_context_two.values())[0]:
        sto.append(wordnet.synsets(tupl[0])[0])
    synsets_two.update({list(phrase_context_two.keys())[0]: sto})
    sto = []
    for syns in list(synsets_one.values())[0]:
        max_sim = 0
        for syns_compare in list(synsets_two.values())[0]:
            if syns.wup_similarity(syns_compare) is not None:
                if (syns.wup_similarity(syns_compare) > max_sim):
                    max_sim = syns.wup_similarity(syns_compare)
        sto.append(max_sim)
    print (sto)
    vector_one = np.asarray(sto)
    print (vector_one)
    sto = []
    for syns in list(synsets_two.values())[0]:
        max_sim = 0
        for syns_compare in list(synsets_one.values())[0]:
            if syns.wup_similarity(syns_compare) is not None:
                if (syns.wup_similarity(syns_compare) > max_sim):
                    max_sim = syns.wup_similarity(syns_compare)
        sto.append(max_sim)
    print (sto)
    vector_two = np.asarray(sto)
    print (vector_two)
    sto = []
    cos = np.dot(vector_one, vector_two) / (np.sqrt(np.dot(vector_one, vector_one)) * np.sqrt(np.dot(vector_two, vector_two)))
    print (cos)


######
######
print(noun_search('hummingbird.txt'))
print(key_phrases('hummingbird.txt'))
print(suggest_tags('hummingbird.txt'))

    
        

    
