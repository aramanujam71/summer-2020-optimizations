import http.client, urllib.request, urllib.parse, urllib.error, base64
import json

# Returns the required authentication parameters to return the scripts
# Subscription Key is static while the authorization code must be renewed
# ADD POSTMAN AUTHENTICATION SCHEMA (TO-DO)
def get_headers():
    headers = {
        'Bb-Api-Subscription-Key': "d9fd673842e54a7e906507a18df3dfc6",
        'Authorization': "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IjREVjZzVkxIM0FtU1JTbUZqMk04Wm5wWHU3WSJ9.eyJuYW1laWQiOiJlMjdjZDdmYS1lMzAzLTRiYWMtYTE4Ni0yY2UzMmIyMWI1NjQiLCJhcHBsaWNhdGlvbmlkIjoiYTA1NmNhNmItYTNhOC00YWM3LWIzMjUtOTk3NjY2MzA2ZTUyIiwiZW52aXJvbm1lbnRpZCI6InAtdm5WQWJEdGZ1MEd5TXFTRG1HLV9xdyIsImVudmlyb25tZW50bmFtZSI6IlNLWSBEZXZlbG9wZXIgQ29ob3J0IEVudmlyb25tZW50IDEiLCJsZWdhbGVudGl0eWlkIjoicC1nVnFyX3lPU2tVcXNFS3RtLVpwSm5BIiwibGVnYWxlbnRpdHluYW1lIjoiU0tZIERldmVsb3BlciBDb2hvcnQiLCJ6b25lIjoicC11c2EwMSIsImlzcyI6Imh0dHBzOi8vb2F1dGgyLnNreS5ibGFja2JhdWQuY29tLyIsImF1ZCI6ImJsYWNrYmF1ZCIsImV4cCI6MTYwNTIyODU3NSwibmJmIjoxNjA1MjI0OTc1fQ.HhzTqdofcy9xDTho1hJNj_1mf5McAa8STbI8aYhND7L0kJ_-hJ40ljwMWAiXUluTtrB6cwpSM4pfdaeO7BKOMp8FnwIqIQd-QRCxL5MT1JOjrrUyH9qJZkeQ8YWhGCjsVUJ4y04O_8ieP3ytR3qTKZULKJzzdRPQjF_B6QDyUNOGhOHFIz1Wb5Z0vpch4FshgH3qdHnrGudKQyEO87L7DlIPLf4rxzYpNR-TTD0F_16cXMsDdwpJezcxYYXzflQrB5OhwwHsnvC_l0Ty0djgWSXEmFPyisevGIrax1xlQP00SMgkQESWiuT7WfyTAJtKHhRoAqy0CvZd-WK-BfzcWQ",
    }
    return headers

# A function that returns the parameters to parse by
# Use the limit and offset to get ALL of the constituents
def get_parameters(limit, offset, c_code = [], c_id = [], cfc = [], fields=[],
                   f_status, i_deceased = False, i_inactive = True, list_id = "",
                   p_code = "", date_added = "", l_mod = "", sort_t = "", sort = []):

    raw_parameters = {
        'constituent_code': c_code,
        'constituent_id': c_id,
        'custom_field_category': cfc,
        'fields': fields,
        'fundraiser_status': f_status,
        'include_deceased': i_deceased,
        'include_inactive': i_inactive,
        'list_id': list_id,
        'postal_code': p_code,
        'date_added': date_added,
        'last_modified': l_mod,
        'sort_token': sort_t,
        'sort': sort,
        'limit': limit,
        'offset': offset,
    }

    for k, v in raw_parameters.items():
        if type(v) is list or type(v) is str and not v:
            del raw_parameters[k]

    return raw_parameters
    
# returns the raw json that will be parsed by the main utility function (initialize_constituent_list)
def get_json(limit, offset):
    
    headers = get_headers()
    
    params = urlib.parse.urlencode(get_parameters(limit,offset))

    try:
        conn = http.client.HTTPSConnection('api.sky.blackbaud.com')
        conn.request("GET", "/constituent/v1/constituents?%s" % params, "{body}", headers)
        response = conn.getresponse()
        data = response.read()
        parse_json(data)
        conn.close()
    except Exception as e:
        print("[Errno {0}] {1}".format(e.errno, e.strerror))

    return data
    
# Driver function
def initalize_constituent_list(count):
    MAX = 500
    data_paginated = []
    off = 0
    while count > 0:
        if count > MAX:
            data_paginated.append(get_json(MAX, off))
            off += count
            count -= MAX                   
        else:
            data_paginated.append(get_json(count, off))
            count -= MAX


    
    for i in data_paginated:
        data_dict = json.loads(i)
        for k, v in data_dict.items():
            if k == "value":
                
        
    
    


def parse_json(data):
    data_dict = json.loads(data)
    for x in data_dict:
        if x == "count" or x == "next_link":
            print(x, data_dict.get(x))
            print("--------------------------")
        elif x == "value":
            create_constituents(data_dict.get(x))

def create_constituents(constituent_data):
    constituent_arr = []
    for i in constituent_data:
        constituent_arr.extend(Constituent (i.get("name"), i.get("address").get("formatted_address"), i.get("id")))
    print_all(constituent_arr)

def print_all(constituent_objs):
    for constituent in constituent_objs:
        constituent.toString()
        print(".............")

class Constituent:

    def __init__(constituent, name, email, iD):
        constituent.iD = iD
        constituent.name = name
        constituent.email = email

    def toString():
        print("id ", iD)
        print("name ", name)
        print("email ", email)



# TO DO #
#fix authorization problem (add functionality in script to get the bearer token automatically so we don't need to manually update every hour)
# postprocess the response with edge case
# upload processed data into ghost accounts
