<?php
// This sample uses the PEAR HTTP client from http://pear.php.net/package/HTTP_Request2
require_once 'HTTP/Request2.php';

$request = new Http_Request2('https://api.sky.blackbaud.com/constituent/v1/constituents');
$url = $request->getUrl();

$headers = array(
    // Request headers
   'Bb-Api-Subscription-Key' => '{subscription key}',
   'Authorization' => 'Bearer {access token}',
);

$request->setHeader($headers);

$parameters = array(
    // Request parameters
    'constituent_code' => '{array}',
    'constituent_id' => '{array}',
    'custom_field_category' => '{array}',
    'fields' => '{array}',
    'fundraiser_status' => '{array}',
    'include_deceased' => '{boolean}',
    'include_inactive' => '{boolean}',
    'list_id' => '{string}',
    'postal_code' => '{array}',
    'date_added' => '{string}',
    'last_modified' => '{string}',
    'sort_token' => '{string}',
    'sort' => '{array}',
    'limit' => '{integer}',
    'offset' => '{integer}',
);

$url->setQueryVariables($parameters);

$request->setMethod(HTTP_Request2::METHOD_GET);

// Request body
$request->setBody("{body}");

try
{
    $response = $request->send();
    echo $response->getBody();
}
catch (HttpException $ex)
{
    echo $ex;
}

?>