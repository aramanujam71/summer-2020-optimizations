<?php
// This sample uses the PEAR HTTP client from http://pear.php.net/package/HTTP_Request2
require_once 'HTTP/Request2.php';

$request = new Http_Request2('https://api.sky.blackbaud.com/constituent/v1/constituents');
$url = $request->getUrl();

$headers = array(
    // Request headers
   'Content-Type' => 'application/json',
   'Bb-Api-Subscription-Key' => '{subscription key}',
   'Authorization' => 'Bearer {access token}',
);

$request->setHeader($headers);

$parameters = array(
    // Request parameters
);

$url->setQueryVariables($parameters);

$request->setMethod(HTTP_Request2::METHOD_POST);

// Request body
$request->setBody("{body}");

try
{
    $response = $request->send();
    echo $response->getBody();
}
catch (HttpException $ex)
{
    echo $ex;
}

?>