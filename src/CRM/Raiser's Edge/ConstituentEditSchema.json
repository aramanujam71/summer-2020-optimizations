{
  "description": "Constituents are the individuals and organizations who support your organization by contributing time, money, and resources. The constituent entity stores information about donors, prospects, volunteers, general supporters, and more.",
  "type": "object",
  "properties": {
    "birthdate": {
      "description": "The constituent's birthdate. For individuals only.",
      "type": "object",
      "properties": {
        "d": {
          "format": "int32",
          "description": "The day in the fuzzy date.",
          "type": "integer"
        },
        "m": {
          "format": "int32",
          "description": "The month in the fuzzy date.",
          "type": "integer"
        },
        "y": {
          "format": "int32",
          "description": "The year in the fuzzy date.",
          "type": "integer"
        }
      },
      "x-display-name": "Fuzzy date",
      "x-display-id": "FuzzyDate"
    },
    "deceased": {
      "description": "Indicates whether the constituent is deceased. For individuals only.",
      "type": "boolean"
    },
    "deceased_date": {
      "description": "The date when the constituent died. For individuals only.",
      "type": "object",
      "properties": {
        "d": {
          "format": "int32",
          "description": "The day in the fuzzy date.",
          "type": "integer"
        },
        "m": {
          "format": "int32",
          "description": "The month in the fuzzy date.",
          "type": "integer"
        },
        "y": {
          "format": "int32",
          "description": "The year in the fuzzy date.",
          "type": "integer"
        }
      },
      "x-display-name": "Fuzzy date",
      "x-display-id": "FuzzyDate"
    },
    "first": {
      "description": "The constituent's first name. For individuals only. Character limit: 50.",
      "maxLength": 50,
      "minLength": 0,
      "type": "string"
    },
    "former_name": {
      "description": "The constituent's former name. For individuals only. Character limit: 100.",
      "maxLength": 100,
      "minLength": 0,
      "type": "string"
    },
    "gender": {
      "description": "The constituent's gender. Available values are the entries in the <a href=\"https://developer.sky.blackbaud.com/docs/services/56b76470069a0509c8f1c5b3/operations/ListGenders\"><b>Gender</b></a> table. This property defaults to <i>Unknown</i> if no value is provided. For individuals only.",
      "type": "string"
    },
    "gives_anonymously": {
      "description": "Indicates whether the constituent gives anonymously.",
      "type": "boolean"
    },
    "inactive": {
      "description": "Indicates whether the constituent is inactive.",
      "type": "boolean"
    },
    "last": {
      "description": "The constituent's last name. For individuals only. Character limit: 100.",
      "maxLength": 100,
      "minLength": 0,
      "type": "string"
    },
    "lookup_id": {
      "description": "The user-defined identifier for the constituent.",
      "type": "string"
    },
    "marital_status": {
      "description": "The constituent's marital status. Available values are the entries in the <a href=\"https://developer.sky.blackbaud.com/docs/services/56b76470069a0509c8f1c5b3/operations/ListMaritalStatuses\"><b>Marital Status</b></a> table.  For individuals only.",
      "type": "string"
    },
    "middle": {
      "description": "The constituent's middle name. For individuals only. Character limit: 50.",
      "maxLength": 50,
      "minLength": 0,
      "type": "string"
    },
    "name": {
      "description": "If the constituent's <code>type</code> is <i>Individual</i>, this is a computed field that does not apply to edit operations. If the <code>type</code> is <i>Organization</i>, this field cannot be changed to null and represents the organization's name. Character limit: 60.",
      "type": "string"
    },
    "preferred_name": {
      "description": "The constituent's preferred name. For individuals only. Character limit: 50.",
      "maxLength": 50,
      "minLength": 0,
      "type": "string"
    },
    "suffix": {
      "description": "The constituent's primary suffix. Available values are the entries in the <a href=\"https://developer.sky.blackbaud.com/docs/services/56b76470069a0509c8f1c5b3/operations/ListSuffixes\"><b>Suffixes</b></a> table. For individuals only.",
      "type": "string"
    },
    "suffix_2": {
      "description": "The constituent's secondary suffix. Available values are the entries in the <a href=\"https://developer.sky.blackbaud.com/docs/services/56b76470069a0509c8f1c5b3/operations/ListSuffixes\"><b>Suffixes</b></a> table. For individuals only.",
      "type": "string"
    },
    "title": {
      "description": "The constituent's primary title. Available values are the entries in the <a href=\"https://developer.sky.blackbaud.com/docs/services/56b76470069a0509c8f1c5b3/operations/ListTitles\"><b>Titles</b></a> table. For individuals only.",
      "type": "string"
    },
    "title_2": {
      "description": "The constituent's secondary title. Available values are the entries in the <a href=\"https://developer.sky.blackbaud.com/docs/services/56b76470069a0509c8f1c5b3/operations/ListTitles\"><b>Titles</b></a> table. For individuals only.",
      "type": "string"
    }
  },
  "x-hidden": true,
  "x-display-name": "Edit constituent",
  "x-display-id": "EditConstituent"
}